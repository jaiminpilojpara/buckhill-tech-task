<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Http\Controllers\ApiController;
use App\Http\Requests\IndexProductRequest;
use App\Http\Requests\StoreProductRequest;
use App\Http\Requests\UpdateProductRequest;
use Illuminate\Http\JsonResponse;

class ProductController extends ApiController
{
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth:api', ['except' => ['index', 'show']]);
    }

    /**
     * @OA\Get(
     *     path="/api/v1/products",
     *     summary="Fetch product List",
     *     description="Fetch product List",
     *     operationId="productList",
     *     tags={"Products"},
     *     @OA\Parameter(
     *        name="page",
     *        in="query",
     *        required=false,
     *        @OA\Schema(
     *             type="integer"
     *        )
     *     ),
     *     @OA\Parameter(
     *        name="limit",
     *        in="query",
     *        required=false,
     *        @OA\Schema(
     *             type="integer"
     *        )
     *     ),
     *     @OA\Parameter(
     *        name="sortBy",
     *        in="query",
     *        required=false,
     *        @OA\Schema(
     *             type="string"
     *        )
     *     ),
     *     @OA\Parameter(
     *        name="desc",
     *        in="query",
     *        required=false,
     *        @OA\Schema(
     *             type="boolean"
     *        )
     *     ),
     *     @OA\Parameter(
     *        name="brand_id",
     *        in="query",
     *        required=false,
     *        @OA\Schema(
     *             type="string"
     *        )
     *     ),
     *     @OA\Parameter(
     *        name="category_id",
     *        in="query",
     *        required=false,
     *        @OA\Schema(
     *             type="string"
     *        )
     *     ),
     *     @OA\Parameter(
     *        name="price_min",
     *        in="query",
     *        required=false,
     *        @OA\Schema(
     *             type="integer"
     *        )
     *     ),
     *     @OA\Parameter(
     *        name="price_max",
     *        in="query",
     *        required=false,
     *        @OA\Schema(
     *             type="integer"
     *        )
     *     ),
     *     @OA\Parameter(
     *        name="title",
     *        in="query",
     *        required=false,
     *        @OA\Schema(
     *             type="string"
     *        )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="OK"
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="Unauthenticated"
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Bad Request"
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="not found"
     *     ),
     *     @OA\Response(
     *         response=403,
     *         description="Forbidden"
     *     )
     * )
     *
     * Display a listing of the resource.
     *
     * @param  App\Http\Requests\IndexProductRequest  $request
     */
    public function index(IndexProductRequest $request): JsonResponse
    {
        $desc = $request->has('desc') ? filter_var($request->desc, FILTER_VALIDATE_BOOLEAN) : true;

        $productData = Product::orderBy($request->sortBy ?? 'updated_at', $desc ? 'DESC' : 'ASC')
                                ->when($request->brand_id, fn($query) => $query->where('brand_id', $request->brand_id))
                                ->when($request->category_id, fn($query) => $query->where('category_id', $request->category_id))
                                ->when($request->price_min && $request->price_max, fn($query) => $query->whereBetween('price', [$request->price_min, $request->price_max]))
                                ->when($request->title, fn($query) => $query->where('title', 'LIKE', '%'.$request->title.'%'))
                                ->with(['brand', 'category', 'file'])
                                ->paginate($request->limit ?? 10);

        return $this->successResponse($productData, 'Product data fetch successfully');
    }

    /**
     * @OA\Post(
     *     path="/api/v1/products",
     *     summary="Create product",
     *     description="Create product",
     *     operationId="productCreate",
     *     tags={"Products"},
     *     security={{"bearer_token":{}}},
     *     @OA\RequestBody(
     *         required=true,
     *         description="Pass title to create product",
     *         @OA\JsonContent(
     *             required={"brand_id","category_id","file_id","title","price","description"},
     *             @OA\Property(property="brand_id", type="string", example="brand_id"),
     *             @OA\Property(property="category_id", type="string", example="category_id"),
     *             @OA\Property(property="file_id", type="string", example="file_id"),
     *             @OA\Property(property="title", type="string", example="Flate White"),
     *             @OA\Property(property="price", type="number", example="99.99"),
     *             @OA\Property(property="description", type="string", example="Flate White is really good coffee. i love it")
     *         ),
     *     ),
     *     @OA\Response(
     *         response=201,
     *         description="Success",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *         )
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="Unauthenticated"
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Bad Request"
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="not found"
     *     ),
     *     @OA\Response(
     *         response=403,
     *         description="Forbidden"
     *     )
     * )
     *
     * Store a newly created product in storage.
     *
     * @param  App\Http\Requests\StoreProductRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreProductRequest $request): JsonResponse
    {
        $product = Product::create($request->validated());

        return $this->successResponse($product, 'Product created successfully', 201);
    }

    /**
     * @OA\Get(
     *     path="/api/v1/products/{uuid}",
     *     summary="Fetch a product",
     *     description="Fetch a product",
     *     operationId="productFetch",
     *     tags={"Products"},
     *     @OA\Parameter(
     *        name="uuid",
     *        in="path",
     *        required=true,
     *        @OA\Schema(
     *             type="string"
     *        )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="OK"
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="Unauthenticated"
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Bad Request"
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="not found"
     *     ),
     *     @OA\Response(
     *         response=403,
     *         description="Forbidden"
     *     )
     * )
     *
     * Display the specified resource.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product): JsonResponse
    {
        return $this->successResponse($product->with(['brand','category','file'])->first(), 'Product data fetch successfully');
    }

    /**
     * @OA\Put(
     *     path="/api/v1/products/{uuid}",
     *     summary="Update product",
     *     description="Update product",
     *     operationId="productUpdate",
     *     tags={"Products"},
     *     security={{"bearer_token":{}}},
     *     @OA\Parameter(
     *        name="uuid",
     *        in="path",
     *        required=true,
     *        @OA\Schema(
     *             type="string"
     *        )
     *     ),
     *     @OA\RequestBody(
     *         required=true,
     *         description="Pass title to Update product",
     *         @OA\JsonContent(
     *             @OA\Property(property="brand_id", type="string", example="brand_id"),
     *             @OA\Property(property="category_id", type="string", example="category_id"),
     *             @OA\Property(property="file_id", type="string", example="file_id"),
     *             @OA\Property(property="title", type="string", example="Flate White"),
     *             @OA\Property(property="price", type="number", example="99.99"),
     *             @OA\Property(property="description", type="string", example="Flate White is really good coffee. i love it")
     *         ),
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="OK"
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="Unauthenticated"
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Bad Request"
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="not found"
     *     ),
     *     @OA\Response(
     *         response=403,
     *         description="Forbidden"
     *     )
     * )
     *
     * Update the specified resource in storage.
     *
     * @param  App\Http\Requests\UpdateProductRequest  $request
     * @param  \App\Models\Product  $product
     */
    public function update(UpdateProductRequest $request, Product $product): JsonResponse
    {
        if(empty($request->all())){
            return $this->errorResponse("No data found for update", 422);
        }

        $product->update($request->validated());

        return $this->successResponse($product, 'Product updated successfully');
    }

    /**
     * @OA\Delete(
     *     path="/api/v1/products/{uuid}",
     *     summary="Delete a product",
     *     description="Delete a product",
     *     operationId="productDelete",
     *     tags={"Products"},
     *     security={{"bearer_token":{}}},
     *     @OA\Parameter(
     *        name="uuid",
     *        in="path",
     *        required=true,
     *        @OA\Schema(
     *             type="string"
     *        )
     *     ),
     *     @OA\Response(
     *         response=204,
     *         description="No Content"
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="Unauthenticated"
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Bad Request"
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="not found"
     *     ),
     *     @OA\Response(
     *         response=403,
     *         description="Forbidden"
     *     )
     * )
     *
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Product  $product
     */
    public function destroy(Product $product): JsonResponse
    {
        $product->delete();
        return $this->successResponse(null, 'Product deleted successfully', 204);
    }
}
