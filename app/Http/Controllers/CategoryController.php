<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Http\Controllers\ApiController;
use App\Http\Requests\IndexCategoryRequest;
use App\Http\Requests\StoreCategoryRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
class CategoryController extends ApiController
{
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth:api', ['except' => ['index', 'show']]);
    }

    /**
     * @OA\Get(
     *     path="/api/v1/categories",
     *     summary="Fetch category List",
     *     description="Fetch category List",
     *     operationId="categoryList",
     *     tags={"Category"},
     *     @OA\Parameter(
     *        name="title",
     *        in="query",
     *        required=false,
     *        @OA\Schema(
     *             type="string"
     *        )
     *     ),
     *     @OA\Parameter(
     *        name="page",
     *        in="query",
     *        required=false,
     *        @OA\Schema(
     *             type="integer"
     *        )
     *     ),
     *     @OA\Parameter(
     *        name="limit",
     *        in="query",
     *        required=false,
     *        @OA\Schema(
     *             type="integer"
     *        )
     *     ),
     *     @OA\Parameter(
     *        name="sortBy",
     *        in="query",
     *        required=false,
     *        @OA\Schema(
     *             type="string"
     *        )
     *     ),
     *     @OA\Parameter(
     *        name="desc",
     *        in="query",
     *        required=false,
     *        @OA\Schema(
     *             type="boolean"
     *        )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="OK"
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="Unauthenticated"
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Bad Request"
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="not found"
     *     ),
     *     @OA\Response(
     *         response=403,
     *         description="Forbidden"
     *     )
     * )
     *
     * Display a listing of the resource.
     *
     * @param  App\Http\Requests\IndexCategoryRequest  $request
     */
    public function index(IndexCategoryRequest $request): JsonResponse
    {
        $desc = $request->has('desc') ? filter_var($request->desc, FILTER_VALIDATE_BOOLEAN) : true;

        $categoryData = Category::orderBy($request->sortBy ?? 'updated_at', $desc ? 'DESC' : 'ASC')
                            ->when($request->title, fn ($query) => $query->where('title', 'LIKE', '%' . $request->title . '%'))
                            ->paginate($request->limit ?? 10);

        return $this->successResponse($categoryData, 'Category data fetch successfully');

    }

    /**
     * @OA\Post(
     *     path="/api/v1/categories",
     *     summary="Create category",
     *     description="Create category",
     *     operationId="categoryCreate",
     *     tags={"Category"},
     *     security={{"bearer_token":{}}},
     *     @OA\RequestBody(
     *         required=true,
     *         description="Pass title to create category",
     *         @OA\JsonContent(
     *             required={"title"},
     *             @OA\Property(property="title", type="string", example="Coffee")
     *         ),
     *     ),
     *     @OA\Response(
     *         response=201,
     *         description="Success",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *         )
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="Unauthenticated"
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Bad Request"
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="not found"
     *     ),
     *     @OA\Response(
     *         response=403,
     *         description="Forbidden"
     *     )
     * )
     *
     * Store a newly created brand in storage.
     *
     * @param  App\Http\Requests\StoreCategoryRequest  $request
     */
    public function store(StoreCategoryRequest $request): JsonResponse
    {
        $category = Category::create($request->validated());
        return $this->successResponse($category, 'Category created successfully', 201);
    }

    /**
     * @OA\Get(
     *     path="/api/v1/categories/{uuid}",
     *     summary="Fetch a category",
     *     description="Fetch a category",
     *     operationId="categoryFetch",
     *     tags={"Category"},
     *     @OA\Parameter(
     *        name="uuid",
     *        in="path",
     *        required=true,
     *        @OA\Schema(
     *             type="string"
     *        )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="OK"
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="Unauthenticated"
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Bad Request"
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="not found"
     *     ),
     *     @OA\Response(
     *         response=403,
     *         description="Forbidden"
     *     )
     * )
     *
     * Display the specified resource.
     *
     * @param  \App\Models\Category  $category
     */
    public function show(Category $category): JsonResponse
    {
        return $this->successResponse($category, 'Category data fetch successfully');
    }

    /**
     * @OA\Put(
     *     path="/api/v1/categories/{uuid}",
     *     summary="Update category",
     *     description="Update category",
     *     operationId="categoryUpdate",
     *     tags={"Category"},
     *     security={{"bearer_token":{}}},
     *     @OA\Parameter(
     *        name="uuid",
     *        in="path",
     *        required=true,
     *        @OA\Schema(
     *             type="string"
     *        )
     *     ),
     *     @OA\RequestBody(
     *         required=true,
     *         description="Pass title to Update category",
     *         @OA\JsonContent(
     *             required={"title"},
     *             @OA\Property(property="title", type="string", example="Coffee")
     *         ),
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="OK"
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="Unauthenticated"
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Bad Request"
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="not found"
     *     ),
     *     @OA\Response(
     *         response=403,
     *         description="Forbidden"
     *     )
     * )
     *
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Category  $category
     */
    public function update(Request $request, Category $category): JsonResponse
    {
        $category->title = $request->title ?? $category->title;
        $category->save();
        return $this->successResponse($category, 'Category updated successfully');
    }

    /**
     * @OA\Delete(
     *     path="/api/v1/categories/{uuid}",
     *     summary="Delete a category",
     *     description="Delete a category",
     *     operationId="categoryDelete",
     *     tags={"Category"},
     *     security={{"bearer_token":{}}},
     *     @OA\Parameter(
     *        name="uuid",
     *        in="path",
     *        required=true,
     *        @OA\Schema(
     *             type="string"
     *        )
     *     ),
     *     @OA\Response(
     *         response=204,
     *         description="No Content"
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="Unauthenticated"
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Bad Request"
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="not found"
     *     ),
     *     @OA\Response(
     *         response=403,
     *         description="Forbidden"
     *     )
     * )
     *
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Category  $category
     */
    public function destroy(Category $category): JsonResponse
    {
        $category->delete();
        return $this->successResponse(null, 'Category deleted successfully', 204);
    }

}
