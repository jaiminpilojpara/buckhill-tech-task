<?php

namespace App\Http\Controllers;

use App\Models\Brand;
use App\Http\Controllers\ApiController;
use App\Http\Requests\IndexBrandRequest;
use App\Http\Requests\StoreBrandRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class BrandController extends ApiController
{
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['index', 'show']]);
    }

    /**
     * @OA\Get(
     *     path="/api/v1/brands",
     *     summary="Fetch brand List",
     *     description="Fetch brand List",
     *     operationId="brandList",
     *     tags={"Brands"},
     *     @OA\Parameter(
     *        name="title",
     *        in="query",
     *        required=false,
     *        @OA\Schema(
     *             type="string"
     *        )
     *     ),
     *     @OA\Parameter(
     *        name="page",
     *        in="query",
     *        required=false,
     *        @OA\Schema(
     *             type="integer"
     *        )
     *     ),
     *     @OA\Parameter(
     *        name="limit",
     *        in="query",
     *        required=false,
     *        @OA\Schema(
     *             type="integer"
     *        )
     *     ),
     *     @OA\Parameter(
     *        name="sortBy",
     *        in="query",
     *        required=false,
     *        @OA\Schema(
     *             type="string"
     *        )
     *     ),
     *     @OA\Parameter(
     *        name="desc",
     *        in="query",
     *        required=false,
     *        @OA\Schema(
     *             type="boolean"
     *        )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="OK"
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="Unauthenticated"
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Bad Request"
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="not found"
     *     ),
     *     @OA\Response(
     *         response=403,
     *         description="Forbidden"
     *     )
     * )
     *
     * Display a listing of the resource.
     *
     * @param  App\Http\Requests\IndexBrandRequest  $request
     */
    public function index(IndexBrandRequest $request): JsonResponse
    {
        $desc = $request->has('desc') ? filter_var($request->desc, FILTER_VALIDATE_BOOLEAN) : true;

        $brandData = Brand::orderBy($request->sortBy ?? 'updated_at', $desc ? 'DESC' : 'ASC')
                            ->when($request->title, fn ($query) => $query->where('title', 'LIKE', '%' . $request->title . '%'))
                            ->paginate($request->limit ?? 10);

        return $this->successResponse($brandData, 'Brand data fetch successfully');
    }

    /**
     * @OA\Post(
     *     path="/api/v1/brands",
     *     summary="Create brand",
     *     description="Create brand",
     *     operationId="brandCreate",
     *     tags={"Brands"},
     *     security={{"bearer_token":{}}},
     *     @OA\RequestBody(
     *         required=true,
     *         description="Pass title to create brand",
     *         @OA\JsonContent(
     *             required={"title"},
     *             @OA\Property(property="title", type="string", example="Starbucks")
     *         ),
     *     ),
     *     @OA\Response(
     *         response=201,
     *         description="Success",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *         )
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="Unauthenticated"
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Bad Request"
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="not found"
     *     ),
     *     @OA\Response(
     *         response=403,
     *         description="Forbidden"
     *     )
     * )
     *
     * Store a newly created brand in storage.
     *
     * @param  App\Http\Requests\StoreBrandRequest  $request
     */
    public function store(StoreBrandRequest $request): JsonResponse
    {
        $brand = Brand::create($request->validated());
        return $this->successResponse($brand, 'Brand created successfully', 201);
    }

    /**
     * @OA\Get(
     *     path="/api/v1/brands/{uuid}",
     *     summary="Fetch a brand",
     *     description="Fetch a brand",
     *     operationId="brandFetch",
     *     tags={"Brands"},
     *     @OA\Parameter(
     *        name="uuid",
     *        in="path",
     *        required=true,
     *        @OA\Schema(
     *             type="string"
     *        )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="OK"
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="Unauthenticated"
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Bad Request"
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="not found"
     *     ),
     *     @OA\Response(
     *         response=403,
     *         description="Forbidden"
     *     )
     * )
     *
     * Display the specified resource.
     *
     * @param  \App\Models\Brand  $brand
     */
    public function show(Brand $brand): JsonResponse
    {
        return $this->successResponse($brand, 'Brand data fetch successfully');
    }

    /**
     * @OA\Put(
     *     path="/api/v1/brands/{uuid}",
     *     summary="Update brand",
     *     description="Update brand",
     *     operationId="brandUpdate",
     *     tags={"Brands"},
     *     security={{"bearer_token":{}}},
     *     @OA\Parameter(
     *        name="uuid",
     *        in="path",
     *        required=true,
     *        @OA\Schema(
     *             type="string"
     *        )
     *     ),
     *     @OA\RequestBody(
     *         required=true,
     *         description="Pass title to Update brand",
     *         @OA\JsonContent(
     *             required={"title"},
     *             @OA\Property(property="title", type="string", example="Starbucks")
     *         ),
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="OK"
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="Unauthenticated"
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Bad Request"
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="not found"
     *     ),
     *     @OA\Response(
     *         response=403,
     *         description="Forbidden"
     *     )
     * )
     *
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Brand  $brand
     */
    public function update(Request $request, Brand $brand): JsonResponse
    {
        $brand->title = $request->title ?? $brand->title;
        $brand->save();

        return $this->successResponse($brand, 'Brand updated successfully');
    }

    /**
     * @OA\Delete(
     *     path="/api/v1/brands/{uuid}",
     *     summary="Delete a brand",
     *     description="Delete a brand",
     *     operationId="brandDelete",
     *     tags={"Brands"},
     *     security={{"bearer_token":{}}},
     *     @OA\Parameter(
     *        name="uuid",
     *        in="path",
     *        required=true,
     *        @OA\Schema(
     *             type="string"
     *        )
     *     ),
     *     @OA\Response(
     *         response=204,
     *         description="No Content"
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="Unauthenticated"
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Bad Request"
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="not found"
     *     ),
     *     @OA\Response(
     *         response=403,
     *         description="Forbidden"
     *     )
     * )
     *
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Brand  $brand
     */
    public function destroy(Brand $brand): JsonResponse
    {
        $brand->delete();
        return $this->successResponse(null, 'Brand deleted successfully', 204);
    }
}
