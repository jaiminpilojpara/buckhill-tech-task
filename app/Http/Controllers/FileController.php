<?php

namespace App\Http\Controllers;

use App\Models\File;
use App\Http\Controllers\ApiController;
use App\Http\Requests\StoreFileRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;

class FileController extends ApiController
{
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth:api', ['except' => ['show']]);
    }

    /**
     * @OA\Post(
     *     path="/api/v1/file",
     *     summary="Upload file",
     *     description="Upload file",
     *     operationId="fileUpload",
     *     tags={"File"},
     *     security={{"bearer_token":{}}},
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\MediaType(
     *             mediaType="multipart/form-data",
     *             @OA\Schema(
     *                 @OA\Property(
     *                     description="Select file to upload",
     *                     property="file",
     *                     type="file",
     *                ),
     *                 required={"file"}
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=201,
     *         description="Success",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *         )
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="Unauthenticated"
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Bad Request"
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="not found"
     *     ),
     *     @OA\Response(
     *         response=403,
     *         description="Forbidden"
     *     )
     * )
     *
     * Store a newly created brand in storage.
     *
     * @param  App\Http\Requests\StoreFileRequest  $request
     */
    public function store(StoreFileRequest $request): JsonResponse
    {
        $path = $request->file->store('public/pet-shop');
        $size = $request->file->getSize();
        $type = $request->file->getMimeType();

        $file = File::create([
            'path' => $path,
            'size' => $size,
            'type' => $type,
        ]);

        return $this->successResponse($file, 'File stored successfully', 201);
    }

    /**
     * @OA\Get(
     *     path="/api/v1/file/{uuid}",
     *     summary="Download a file",
     *     description="Download a file",
     *     operationId="downloadFile",
     *     tags={"File"},
     *     @OA\Parameter(
     *        name="uuid",
     *        in="path",
     *        required=true,
     *        @OA\Schema(
     *             type="string"
     *        )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="OK"
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="Unauthenticated"
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Bad Request"
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="not found"
     *     ),
     *     @OA\Response(
     *         response=403,
     *         description="Forbidden"
     *     )
     * )
     *
     * Display the specified resource.
     *
     * @param  \App\Models\File  $file
     */
    public function show(File $file): Response
    {
        return response()->download(storage_path('app/'.$file->path));
    }
}
