<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class IndexProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'page' => 'integer',
            'limit' => 'integer',
            'desc' => 'in:true,false',
            "sortBy" => 'in:title,created_at,updated_at',
            'brand_id' => 'exists:brands,id',
            'category_id' => 'exists:categories,id',
            'file_id' => 'exists:files,id',
            'price_min' => 'numeric|between:0,999999.99',
            'price_max' => 'numeric|between:0,999999.99'
        ];
    }
}
