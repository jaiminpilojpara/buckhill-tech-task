<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'brand_id' => 'exists:brands,id,deleted_at,NULL',
            'category_id' => 'exists:categories,id,deleted_at,NULL',
            'file_id' => 'exists:files,id',
            'title' => 'string|max:255',
            'price' => 'required|numeric|between:0,999999.99',
            'description' => 'string'
        ];
    }
}
