<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\Uuid;
use App\Models\Brand;
use App\Models\Category;
use App\Models\File;

class Product extends Model
{
    use Uuid, HasFactory, SoftDeletes;

    public $incrementing = false;

    protected $keyType = 'uuid';

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'brand_id',
        'category_id',
        'file_id',
        'title',
        'price',
        'description'
    ];

    /**
     * Get the Brand record associated with the product.
     */
    public function brand()
    {
        return $this->belongsTo(Brand::class);
    }

    /**
     * Get the Category record associated with the product.
     */
    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    /**
     * Get the File record associated with the product.
     */
    public function file()
    {
        return $this->belongsTo(File::class);
    }

}
