<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;

class FileFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $file = UploadedFile::fake()->image('avatar.jpg');
        return [
            'path' => $file->store('public/pet-shop'),
            'size' => $file->getSize(),
            'type' => $file->getMimeType(),
        ];
    }
}
