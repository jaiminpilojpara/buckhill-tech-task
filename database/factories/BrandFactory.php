<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class BrandFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'title' => $this->faker->word(),
            'slug' => $this->faker->slug(),
            // 'title' => $faker->text,
            // 'slug' => $faker->slug,
            // // 'user_id' => factory(App\User::class),
            // // 'keywords' => $faker->text,
            // // 'description' => $faker->text,
            // // 'content' => $faker->paragraph,
        ];
    }
}
