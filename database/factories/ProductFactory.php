<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\Brand;
use App\Models\Category;
use App\Models\File;

class ProductFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'brand_id' => Brand::factory()->create()->id,
            'category_id' => Category::factory()->create()->id,
            'file_id' => File::factory()->create()->id,
            'title' => $this->faker->word(),
            'price' => $this->faker->randomFloat(2,000000,999999),
            'description' => $this->faker->text(),
        ];
    }
}
