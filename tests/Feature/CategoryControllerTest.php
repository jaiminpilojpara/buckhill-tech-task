<?php

namespace Tests\Feature;

use App\Models\Category;
use App\Models\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CategoryControllerTest extends TestCase
{
    use RefreshDatabase;

    public function testStoreCategoryUnauthorizedAccess()
    {
        $this->postJson(route('api.categories.store'), [
            'title' => 'Coffee Test',
        ])
            ->assertUnauthorized();
    }

    public function testStoreUserCanCreateCategory()
    {
        $user = User::factory()->create();
        $this->actingAs($user)
            ->postJson(route('api.categories.store'), [
                'title' => 'Coffee Test',
            ])
            ->assertCreated()
            ->assertJson([
                'data' => [
                    'title' => 'Coffee Test',
                    'slug' => 'coffee-test',
                ]
            ]);

        $this->assertDatabaseHas('categories', ['title' => 'Coffee Test', 'slug' => 'coffee-test']);
    }

    public function testStoreCreateCategoryWithSameTitle()
    {
        $user = User::factory()->create();
        $this->actingAs($user)
            ->postJson(route('api.categories.store'), [
                'title' => 'Coffee Test New',
            ])
            ->assertCreated()
            ->assertJson([
                'data' => [
                    'title' => 'Coffee Test New',
                    'slug' => 'coffee-test-new',
                ]
            ]);

        $this->assertDatabaseHas('categories', ['title' => 'Coffee Test New', 'slug' => 'coffee-test-new']);

        $user = User::factory()->create();
        $this->actingAs($user)
            ->postJson(route('api.categories.store'), [
                'title' => 'Coffee Test New',
            ])
            ->assertStatus(422)
            ->assertJson([
                'message' => [
                    "slug" => [
                        "The slug has already been taken."
                    ]
                ]
            ]);
    }

    public function testStoreCreateCategoryWithoutTitle()
    {
        $user = User::factory()->create();
        $this->actingAs($user)
            ->postJson(route('api.categories.store'), [])
            ->assertStatus(422)
            ->assertJson([
                'message' => [
                    "title" => [
                        "The title field is required."
                    ],
                    "slug" => [
                        "The slug field is required."
                    ]
                ]
            ]);
    }

    public function testUpdateCategoryUnauthorizedAccess()
    {
        $category = Category::factory()->create();
        $this->putJson(route('api.categories.update', ['category' => $category->id]), [
            'title' => 'Coffee Test'
        ])->assertUnauthorized();
    }

    public function testUpdateUserCanUpdateCategory()
    {
        $category = Category::factory()->create();
        $user = User::factory()->create();

        $this->actingAs($user)
            ->putJson(route('api.categories.update', ['category' => $category->id]), [
                'title' => 'Coffee Test'
            ])
            ->assertStatus(200)
            ->assertJson([
                'data' => [
                    'title' => 'Coffee Test',
                ]
            ]);

        $this->assertDatabaseHas('categories', ['id' => $category->id, 'title' => 'Coffee Test']);
    }

    public function testDeleteCategoryUnauthorizedAccess()
    {
        $category = Category::factory()->create();
        $this->deleteJson(route('api.categories.destroy', ['category' => $category->id]), [])
            ->assertUnauthorized();
    }

    public function testDeleteUserCanDeleteCategory()
    {
        $category = Category::factory()->create();
        $user = User::factory()->create();

        $this->actingAs($user)
            ->deleteJson(route('api.categories.destroy', ['category' => $category->id]), [])
            ->assertStatus(204);

        $this->assertDatabaseMissing('categories', ['id' => $category->id, "deleted_at" => null]);
    }

}
