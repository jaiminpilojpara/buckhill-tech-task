<?php

namespace Tests\Feature;

use App\Models\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\UploadedFile;

class FileControllerTest extends TestCase
{
    use RefreshDatabase;

    public function testStoreFileUnauthorizedAccess()
    {
        $this->postJson(route('api.file.store'), [
            'file' => UploadedFile::fake()->image('file.png', 600, 600)
        ])->assertUnauthorized();
    }

    public function testStoreUserCanUploadFile()
    {
        $file = UploadedFile::fake()->image('file.png', 600, 600);
        $user = User::factory()->create();

        $this->actingAs($user)
            ->postJson(route('api.file.store'), [
                'file' => $file
            ])
            ->assertCreated()
            ->assertJson([
                'data' => [
                    'size' => $file->getSize(),
                    'type' => $file->getMimeType(),
                ]
            ]);

        $this->assertDatabaseHas('files', ['size' => $file->getSize(), 'type' => $file->getMimeType()]);
    }

    public function testStoreFileSizeLimitExceed()
    {
        $user = User::factory()->create();

        $this->actingAs($user)
            ->postJson(route('api.file.store'), [
                'file' => UploadedFile::fake()->image('file.png', 600, 600)->size(2050)
            ])
            ->assertStatus(422)
            ->assertJson([
                'message' => [
                    "file" => [
                        "The file must not be greater than 2048 kilobytes."
                    ]
                ]
            ]);
    }
}
