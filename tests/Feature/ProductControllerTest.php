<?php

namespace Tests\Feature;

use App\Models\Brand;
use App\Models\Category;
use App\Models\File;
use App\Models\Product;
use App\Models\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ProductControllerTest extends TestCase
{
    use RefreshDatabase;

    public function testStoreProductUnauthorizedAccess()
    {
        $brand = Brand::factory()->create();
        $category = Category::factory()->create();
        $file = File::factory()->create();

        $this->postJson(route('api.products.store'), [
                'brand_id' => $brand->id,
                'category_id' => $category->id,
                'file_id' => $file->id,
                'title' => 'Flat White',
                'price' => 99.99,
                'description' => 'Flat White is really good coffee. I love it',
            ])
            ->assertUnauthorized();
    }

    public function testStoreUserCanCreateProduct()
    {
        $brand = Brand::factory()->create();
        $category = Category::factory()->create();
        $file = File::factory()->create();
        $user = User::factory()->create();

        $this->actingAs($user)
            ->postJson(route('api.products.store'), [
                'brand_id' => $brand->id,
                'category_id' => $category->id,
                'file_id' => $file->id,
                'title' => 'Flat White',
                'price' => 99.99,
                'description' => 'Flat White is really good coffee. I love it',
            ])
            ->assertCreated()
            ->assertJson([
                'data' => [
                    'title' => 'Flat White',
                    'price' => 99.99,
                    'description' => 'Flat White is really good coffee. I love it',
                ]
            ]);

        $this->assertDatabaseHas('products', ['brand_id' => $brand->id, 'category_id' => $category->id, 'file_id' => $file->id, 'title' => 'Flat White', 'price' => '99.99', 'description' => 'Flat White is really good coffee. I love it']);
    }

    public function testStoreCreateProductWithoutRequiredField()
    {
        $brand = Brand::factory()->create();
        $category = Category::factory()->create();
        $file = File::factory()->create();
        $user = User::factory()->create();

        $this->actingAs($user)
            ->postJson(route('api.products.store'), [])
            ->assertStatus(422)
            ->assertJson([
                'message' => [
                    "brand_id" => [
                        "The brand id field is required."
                    ],
                    "category_id" => [
                        "The category id field is required."
                    ],
                    "file_id" => [
                        "The file id field is required."
                    ],
                    "title" => [
                        "The title field is required."
                    ],
                    "price" => [
                        "The price field is required."
                    ],
                    "description" => [
                        "The description field is required."
                    ],
                ]
            ]);
    }

    public function testStoreCreateProductWithInvalidBrandIdCategoryIdFileId()
    {
        $user = User::factory()->create();

        $this->actingAs($user)
            ->postJson(route('api.products.store'), [
                'brand_id' => '188b3e2b-7646-4b12-b33e-8f6c2329aba7-incorrect-brand_id',
                'category_id' => '188b3e2b-7646-4b12-b33e-8f6c2329aba7-incorrect-category_id',
                'file_id' => '188b3e2b-7646-4b12-b33e-8f6c2329aba7-incorrect-file_id',
                'title' => 'Flat White',
                'price' => 99.99,
                'description' => 'Flat White is really good coffee. I love it',
            ])
            ->assertStatus(422)
            ->assertJson([
                'message' => [
                    "brand_id" => [
                        "The selected brand id is invalid."
                    ],
                    "category_id" => [
                        "The selected category id is invalid."
                    ],
                    "file_id" => [
                        "The selected file id is invalid."
                    ]
                ]
            ]);
    }

    public function testStoreCreateProductWithDeletedBrandIdCategoryIdFileId()
    {
        $brand = Brand::factory()->create();
        $category = Category::factory()->create();
        $file = File::factory()->create();

        $brandId = $brand->id;
        $categoryId = $category->id;
        $fileId = $file->id;

        $brand->delete();
        $category->delete();
        $file->delete();

        $user = User::factory()->create();

        $this->actingAs($user)
            ->postJson(route('api.products.store'), [
                'brand_id' => $brandId,
                'category_id' => $categoryId,
                'file_id' => $fileId,
                'title' => 'Flat White',
                'price' => 99.99,
                'description' => 'Flat White is really good coffee. I love it',
            ])
            ->assertStatus(422)
            ->assertJson([
                'message' => [
                    "brand_id" => [
                        "The selected brand id is invalid."
                    ],
                    "category_id" => [
                        "The selected category id is invalid."
                    ],
                    "file_id" => [
                        "The selected file id is invalid."
                    ]
                ]
            ]);
    }

    public function testStoreCreateProductWithStringPrice()
    {
        $brand = Brand::factory()->create();
        $category = Category::factory()->create();
        $file = File::factory()->create();

        $user = User::factory()->create();

        $this->actingAs($user)
            ->postJson(route('api.products.store'), [
                'brand_id' => $brand->id,
                'category_id' => $category->id,
                'file_id' => $file->id,
                'title' => 'Flat White',
                'price' => "aa99.99",
                'description' => 'Flat White is really good coffee. I love it',
            ])
            ->assertStatus(422)
            ->assertJson([
                'message' => [
                    "price" => [
                        "The price must be a number."
                    ]
                ]
            ]);
    }

    public function testStoreCreateProductWithOutOfRangePrice()
    {
        $brand = Brand::factory()->create();
        $category = Category::factory()->create();
        $file = File::factory()->create();

        $user = User::factory()->create();

        $this->actingAs($user)
            ->postJson(route('api.products.store'), [
                'brand_id' => $brand->id,
                'category_id' => $category->id,
                'file_id' => $file->id,
                'title' => 'Flat White',
                'price' => 999999999,
                'description' => 'Flat White is really good coffee. I love it',
            ])
            ->assertStatus(422)
            ->assertJson([
                'message' => [
                    "price" => [
                        "The price must be between 0 and 999999.99."
                    ]
                ]
            ]);
    }

    public function testUpdateProductUnauthorizedAccess()
    {
        $product = Product::factory()->create();
        $brand = Brand::factory()->create();

        $this->putJson(route('api.products.update', ['product' => $product->id]), [
                'brand_id' => $brand->id,
                'title' => 'Flat White',
                'price' => 99.99,
            ])
            ->assertUnauthorized();
    }

    public function testUpdateUserCanUpdateProduct()
    {
        $product = Product::factory()->create();
        $brand = Brand::factory()->create();
        $user = User::factory()->create();

        $this->actingAs($user)
            ->putJson(route('api.products.update', ['product' => $product->id]), [
                'brand_id' => $brand->id,
                'title' => 'Flat White',
                'price' => 99.99,
            ])
            ->assertStatus(200)
            ->assertJson([
                'data' => [
                    'brand_id' => $brand->id,
                    'title' => 'Flat White',
                    'price' => 99.99
                ]
            ]);

        $this->assertDatabaseHas('products', ['id' => $product->id, 'brand_id' => $brand->id, 'title' => 'Flat White', 'price' => 99.99]);
    }

    public function testUpdateProductWithInvalidBrandIdCategoryIdFileId()
    {
        $product = Product::factory()->create();
        $user = User::factory()->create();

        $this->actingAs($user)
            ->putJson(route('api.products.update', ['product' => $product->id]), [
                'brand_id' => '188b3e2b-7646-4b12-b33e-8f6c2329aba7-incorrect-brand_id',
                'category_id' => '188b3e2b-7646-4b12-b33e-8f6c2329aba7-incorrect-category_id',
                'file_id' => '188b3e2b-7646-4b12-b33e-8f6c2329aba7-incorrect-file_id'
            ])
            ->assertStatus(422)
            ->assertJson([
                'message' => [
                    "brand_id" => [
                        "The selected brand id is invalid."
                    ],
                    "category_id" => [
                        "The selected category id is invalid."
                    ],
                    "file_id" => [
                        "The selected file id is invalid."
                    ]
                ]
            ]);
    }

    public function testUpdateProductWithDeletedBrandIdCategoryIdFileId()
    {
        $product = Product::factory()->create();

        $brand = Brand::factory()->create();
        $category = Category::factory()->create();
        $file = File::factory()->create();

        $brandId = $brand->id;
        $categoryId = $category->id;
        $fileId = $file->id;

        $brand->delete();
        $category->delete();
        $file->delete();

        $user = User::factory()->create();

        $this->actingAs($user)
            ->putJson(route('api.products.update', ['product' => $product->id]), [
                'brand_id' => $brandId,
                'category_id' => $categoryId,
                'file_id' => $fileId,
            ])
            ->assertStatus(422)
            ->assertJson([
                'message' => [
                    "brand_id" => [
                        "The selected brand id is invalid."
                    ],
                    "category_id" => [
                        "The selected category id is invalid."
                    ],
                    "file_id" => [
                        "The selected file id is invalid."
                    ]
                ]
            ]);
    }

    public function testUpdateProductWithStringPrice()
    {
        $product = Product::factory()->create();
        $user = User::factory()->create();

        $this->actingAs($user)
            ->putJson(route('api.products.update', ['product' => $product->id]), [
                'price' => "aa99.99"
            ])
            ->assertStatus(422)
            ->assertJson([
                'message' => [
                    "price" => [
                        "The price must be a number."
                    ]
                ]
            ]);
    }

    public function testUpdateProductWithOutOfRangePrice()
    {
        $product = Product::factory()->create();
        $user = User::factory()->create();

        $this->actingAs($user)
            ->putJson(route('api.products.update', ['product' => $product->id]), [
                'price' => 999999999
            ])
            ->assertStatus(422)
            ->assertJson([
                'message' => [
                    "price" => [
                        "The price must be between 0 and 999999.99."
                    ]
                ]
            ]);
    }

    public function testDeleteProductUnauthorizedAccess()
    {
        $product = Product::factory()->create();
        $this->deleteJson(route('api.products.destroy', ['product' => $product->id]), [])
            ->assertUnauthorized();
    }

    public function testDeleteUserCanDeleteProduct()
    {
        $product = Product::factory()->create();
        $user = User::factory()->create();

        $this->actingAs($user)
            ->deleteJson(route('api.products.destroy', ['product' => $product->id]), [])
            ->assertStatus(204);

        $this->assertDatabaseMissing('products', ['id' => $product->id, "deleted_at" => null]);
    }
}
