<?php

namespace Tests\Feature;

use App\Models\Brand;
use App\Models\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class BrandControllerTest extends TestCase
{
    use RefreshDatabase;

    public function testStoreBrandUnauthorizedAccess()
    {
        $this->postJson(route('api.brands.store'), [
                'title' => 'Starbucks Test',
            ])
            ->assertUnauthorized();
    }

    public function testStoreUserCanCreateBrand()
    {
        $user = User::factory()->create();
        $this->actingAs($user)
            ->postJson(route('api.brands.store'), [
                'title' => 'Starbucks Test',
            ])
            ->assertCreated()
            ->assertJson([
                'data' => [
                    'title' => 'Starbucks Test',
                    'slug' => 'starbucks-test',
                ]
            ]);

        $this->assertDatabaseHas('brands', ['title' => 'Starbucks Test', 'slug' => 'starbucks-test']);
    }

    public function testStoreCreateBrandWithSameTitle()
    {
        $user = User::factory()->create();
        $this->actingAs($user)
            ->postJson(route('api.brands.store'), [
                'title' => 'Starbucks Test New',
            ])
            ->assertCreated()
            ->assertJson([
                'data' => [
                    'title' => 'Starbucks Test New',
                    'slug' => 'starbucks-test-new',
                ]
            ]);

        $this->assertDatabaseHas('brands', ['title' => 'Starbucks Test New', 'slug' => 'starbucks-test-new']);

        $user = User::factory()->create();
        $this->actingAs($user)
            ->postJson(route('api.brands.store'), [
                'title' => 'Starbucks Test New',
            ])
            ->assertStatus(422)
            ->assertJson([
                'message' => [
                    "slug" => [
                        "The slug has already been taken."
                    ]
                ]
            ]);

    }

    public function testStoreCreateBrandWithoutTitle()
    {
        $user = User::factory()->create();
        $this->actingAs($user)
            ->postJson(route('api.brands.store'), [])
            ->assertStatus(422)
            ->assertJson([
                'message' => [
                    "title" => [
                        "The title field is required."
                    ],
                    "slug" => [
                        "The slug field is required."
                    ]
                ]
            ]);
    }

    public function testUpdateBrandUnauthorizedAccess()
    {
        $brand = Brand::factory()->create();
        $this->putJson(route('api.brands.update', ['brand' => $brand->id]),[
            'title' => 'Starbucks Test'
        ])->assertUnauthorized();
    }

    public function testUpdateUserCanUpdateBrand()
    {
        $brand = Brand::factory()->create();
        $user = User::factory()->create();

        $this->actingAs($user)
            ->putJson(route('api.brands.update', ['brand' => $brand->id]),[
                'title' => 'Starbucks Test'
            ])
            ->assertStatus(200)
            ->assertJson([
                'data' => [
                    'title' => 'Starbucks Test',
                ]
            ]);

        $this->assertDatabaseHas('brands', ['id' => $brand->id, 'title' => 'Starbucks Test']);
    }

    public function testDeleteBrandUnauthorizedAccess()
    {
        $brand = Brand::factory()->create();
        $this->deleteJson(route('api.brands.destroy', ['brand' => $brand->id]), [])
                ->assertUnauthorized();
    }

    public function testDeleteUserCanDeleteBrand()
    {
        $brand = Brand::factory()->create();
        $user = User::factory()->create();

        $this->actingAs($user)
            ->deleteJson(route('api.brands.destroy', ['brand' => $brand->id]), [])
            ->assertStatus(204);

        $this->assertDatabaseMissing('brands', ['id' => $brand->id, "deleted_at" => null]);
    }
}
